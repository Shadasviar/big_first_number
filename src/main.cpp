#include <iostream>
#include <bigint.h>
#include <bigint_range.h>

/* Wersja sekwencyjna */
#ifdef SEQ
BigInt isPrime(const BigInt &x) {
  for (auto &i : BigIntRangeIterator(2, x)) {
    if (x % i == 0) return i;
  }
  return 0;
}
#endif

/* Wersja OPENMP*/
#ifdef OPENMP
#include <omp.h>

BigInt isPrime(const BigInt &x) {
  static volatile bool run = true;
  BigInt start(2);
  BigInt res(0);
  static volatile bool first_step = true;

  #pragma omp parallel shared(res, start, run, first_step)
  {
    BigInt i(2);
    BigInt stop(0);
    BigInt step(x / (BigInt(omp_get_num_threads() - 1)));
    if (step < BigInt(omp_get_num_threads())) step = 1;

    #pragma omp critical
    {
      if (first_step) {
        first_step = false;
      } else {
        start = start + step;
        if (start > x) start = x - 1;
      }
      stop = start + step;
      if (stop > x) stop = x - 1;
      i = start;
    }

    while (i < stop && run && i < x) {
      if (x % i == 0) {
        run = false;
        res = i;
        break;
      }
      ++i;
    }
  }
  return res;
}
#endif

/* Wersja na wątkach standardowych */
#ifdef THREADS
#include <algorithm>
#include <thread>
#include <mutex>
#include <vector>
#include <atomic>

BigInt isPrime(const BigInt &x) {
  std::atomic<bool> run = true;
  BigInt i(2), res(0);
  std::mutex m1, m2;

  auto checkNumber = [&]() {
    BigInt y(2);
    auto stop = [&](BigInt &&x) {
      if (!run.load()) return;
      std::lock_guard<std::mutex> l(m1);
      res = x;
      run = false;
    };

    while (run.load()) {
      {
        std::lock_guard<std::mutex> l(m2);
        y = i;
        ++i;
      }
      if (y >= x) stop(0);
      if (x % y == 0) stop(std::move(y));
    }
  };

  std::vector<std::thread> threads;
  for (int j = 0; j < std::thread::hardware_concurrency(); ++j)
    threads.push_back(std::thread(checkNumber));
  for (auto &x: threads) x.join();
  return res;
}
#endif

/* Wersja MPI */
#ifdef MPI

#include <mpi.h>

BigInt isPrime(const BigInt &x) {
  int n_threads, i_thread;
  MPI_Comm_size(MPI_COMM_WORLD, &n_threads);
  MPI_Comm_rank(MPI_COMM_WORLD, &i_thread);
  for (BigIntRangeIterator i(2 + i_thread, x); i < i.end(); i += BigInt(n_threads)) {
    if (x % *i == 0) return *i;
  }
  return 0;
}

#endif

int main(int argc, char** argv)
{
#ifdef MPI
    MPI_Init(&argc, &argv);
    int i_thread, n_threads;
    int n_recieved = 0;
    MPI_Comm_size(MPI_COMM_WORLD, &n_threads);
    MPI_Comm_rank(MPI_COMM_WORLD, &i_thread);
#endif
    BigInt b(argv[1]);

    BigInt y = isPrime(b);

#ifdef MPI
    auto data = y.data();
    int size = data.size();
    MPI_Send(reinterpret_cast<void*>(&size), 1, MPI_UNSIGNED_LONG_LONG, 0, 0, MPI_COMM_WORLD);
    MPI_Send(data.data(), size, MPI_UNSIGNED_CHAR, 0, 1, MPI_COMM_WORLD);

    std::vector<BigInt> ys(n_threads, 0);

    MPI_Status status;
    if (i_thread == 0) {
      while (n_recieved < n_threads) {
        MPI_Recv(&size, 1, MPI_UNSIGNED_CHAR, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
        ys[status.MPI_SOURCE].data().resize(size);
        MPI_Recv(data.data(), size, MPI_UNSIGNED_CHAR, MPI_ANY_SOURCE, 1, MPI_COMM_WORLD, &status);
        ys[status.MPI_SOURCE].data() = data;
        if (ys[status.MPI_SOURCE] > 0) {
          y = ys[status.MPI_SOURCE];
          break;
        }
        ++n_recieved;
      }
#endif

    if (y > 0) {
      std::cout << std::string(b / y) << " " << std::string(b % y) << std::endl;
    }
    std::cout << (y == 0
             ? "Is first"
             : std::string("Is not first: devider = " + std::string(y)))
              << std::endl;

#ifdef MPI
      MPI_Abort(MPI_COMM_WORLD, 0);
    }
    MPI_Finalize();
#endif
    return 0;
}
