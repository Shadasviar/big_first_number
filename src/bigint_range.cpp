#include <bigint_range.h>

bool BigIntRangeIterator::operator!=(const BigIntRangeIterator &rhs) const {
  return _it != rhs._it;
}

bool BigIntRangeIterator::operator==(const BigIntRangeIterator &rhs) const {
  return _it == rhs._it;
}

bool BigIntRangeIterator::operator<(const BigIntRangeIterator &rhs) const {
  return _it < rhs._it;
}

bool BigIntRangeIterator::operator>=(const BigIntRangeIterator &rhs) const {
  return _it >= rhs._it;
}

BigInt const & BigIntRangeIterator::operator*() const {
  return _it;
}

BigIntRangeIterator const & BigIntRangeIterator::operator++() {
  ++_it;
  return *this;
}

BigIntRangeIterator BigIntRangeIterator::begin() const {
  return BigIntRangeIterator(_begin, _end);
}

BigIntRangeIterator BigIntRangeIterator::end() const {
  BigIntRangeIterator ret(_begin, _end);
  ret._it = _end;
  return ret;
}

BigInt BigIntRangeIterator::operator-(const BigIntRangeIterator &rhs) const {
  return _it - rhs._it;
}

BigIntRangeIterator BigIntRangeIterator::operator+(const BigInt &rhs) const {
  BigIntRangeIterator res = *this;
  res._it = res._it + rhs;
  return res;
}

BigIntRangeIterator const & BigIntRangeIterator::operator+=(const BigInt &rhs) {
  _it = _it + rhs;
  return *this;
}

bool operator<(uint64_t lhs, const BigIntRangeIterator &rhs) {
  return BigInt(lhs) < rhs._it;
}

BigInt BigIntRangeIterator::operator[](const BigInt &rhs) const {
  return _begin + rhs;
}
