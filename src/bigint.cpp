#include <bigint.h>
#include <numeric>
#include <algorithm>
#include <cmath>

BigInt::BigInt(std::string s) {
  for(auto c: s) _data.push_back(c - '0');
}

BigInt::BigInt(uint64_t x) : BigInt(std::to_string(x)) {
}

BigInt::BigInt() {}

BigInt::operator std::string() const {
  return std::accumulate
      ( _data.begin(), _data.end(), std::string()
      , [](auto a, auto x){return a += (x + '0');});
}

bool BigInt::operator>=(const BigInt &rhs) const {
  if (rhs._data.size() != _data.size())
    return rhs._data.size() < _data.size();
  for (auto i = 0; i < _data.size(); ++i) {
    if (rhs._data[i] == _data[i]) continue;
    return _data[i] > rhs._data[i];
  }
  return true;
}

bool BigInt::operator==(const BigInt &rhs) const {return _data == rhs._data;}
bool BigInt::operator<(const BigInt &rhs) const {return rhs >= *this && rhs != *this;}
bool BigInt::operator<(uint64_t rhs) const {return *this < BigInt(rhs);}
bool BigInt::operator>(const BigInt &rhs) const {return rhs < *this;}
bool BigInt::operator>(uint64_t rhs) const {return *this > BigInt(rhs);}
bool BigInt::operator<=(const BigInt &rhs) const {return !(rhs < *this);}
bool BigInt::operator!=(const BigInt &rhs) const {return !(*this == rhs);}

bool operator<(uint64_t lhs, const BigInt &rhs) {
  return BigInt(lhs) < rhs;
}

std::tuple<BigInt, BigInt> BigInt::devide(const BigInt &rhs) const {
  BigInt div, rest;
  if (rhs == BigInt(0)) throw std::out_of_range("Zero division");
  for (auto digit : _data) {
    rest._data.push_back(digit);
    if (rest >= rhs) {
      uint64_t div_x = 0;
      BigInt tmp(rhs);
      do {
        tmp = rhs * BigInt(div_x++);
      } while (tmp <= rest);
      div_x -= 2;
      div._data.push_back(div_x);
      tmp = rhs * BigInt(div_x);
      rest = rest - tmp;
      if (rest == BigInt(0)) {
        rest._data.clear();
      }
    }
  }
  if (div._data.size() == 0) div = BigInt(0);
  if (rest._data.size() == 0) rest = BigInt(0);
  return {div, rest};
}

BigInt BigInt::operator/(uint64_t rhs) const {
  return *this / BigInt(rhs);
}

BigInt BigInt::operator/(const BigInt &rhs) const {
  return std::get<0>(devide(rhs));
}

BigInt BigInt::operator%(const BigInt &rhs) const {
  return std::get<1>(devide(rhs));
}

BigInt BigInt::operator-(const BigInt &rhs) const {
  BigInt res;
  if (rhs > *this) {
    throw std::out_of_range("Negative number");
  }
  if (rhs == *this) return BigInt(0);
  uint64_t carry = 0;
  int tmp = 0;
  int size_offset = _data.size() - rhs._data.size();

  for (int i = _data.size() - 1; i >= 0; --i) {
    int j = i - size_offset;
    tmp = _data[i] - carry;
    if (j >= 0) tmp = _data[i] - rhs._data[j] - carry;
    if (tmp < 0) {
      carry = 1;
      tmp = 10 + tmp;
    } else {
      carry = 0;
    }
    res._data.push_back(tmp);
  }
  std::reverse(res._data.begin(), res._data.end());
  int trunc = 0;
  for (auto x : res._data) {
    if (x == 0) ++trunc;
    else break;
  }
  res._data.erase(res._data.begin(), res._data.begin() + trunc);
  return res;
}

BigInt BigInt::operator+(const BigInt &rhs) const {
  BigInt res;
  auto ilhs = this->_data.rbegin();
  auto irhs = rhs._data.rbegin();
  uint64_t carry = 0;

  auto add_num = [&](uint8_t x) {
    uint8_t tmp = 0;
    tmp = x + carry;
    if (tmp > 9) {
      carry = tmp / 10;
      tmp %= 10;
    } else {
      carry = 0;
    }
    res._data.push_back(tmp);
  };

  while (ilhs != _data.rend() && irhs != rhs._data.rend())
    add_num(*ilhs++ + *irhs++);
  while (ilhs != _data.rend()) add_num(*ilhs++);
  while (irhs != rhs._data.rend()) add_num(*irhs++);
  while (carry != 0) add_num(0);

  std::reverse(res._data.begin(), res._data.end());
  return res;
}

BigInt BigInt::operator*(const BigInt &rhs) const {
  BigInt res, buf;
  res._data.resize(rhs._data.size() * _data.size() + 1, 0);
  uint64_t carrige = 0;

  auto addCarry = [&carrige](auto &x) {
    while (carrige > 0) {
      x._data.push_back(carrige % 10);
      carrige /= 10;
    }
  };

  for (int i = rhs._data.size() - 1; i >= 0; --i) {
    buf._data = std::vector<uint8_t>(rhs._data.size() - i - 1, 0);
    carrige = 0;
    for (int j = _data.size() - 1; j >= 0; --j) {
      auto tmp = (rhs._data[i] * _data[j]) + carrige;
      buf._data.push_back(tmp % 10);
      carrige = tmp / 10;
    }
    addCarry(buf);
    carrige = 0;
    res._data.resize(buf._data.size());
    for (int i = 0; i < buf._data.size(); ++i) {
      auto sum = res._data[i] + buf._data[i] + carrige;
      res._data[i] = sum % 10;
      carrige = sum / 10;
    }
    addCarry(res);
  }

  std::reverse(res._data.begin(), res._data.end());
  return res;
}

BigInt const & BigInt::operator--() {
  *this = *this - BigInt(1);
  return *this;
}

BigInt const & BigInt::operator++() {
  *this = *this + 1;
  return *this;
}

BigInt const & BigInt::operator<<=(uint64_t rhs) {
  *this = *this * BigInt(pow(2, rhs));
  return *this;
}

BigInt BigInt::operator<<(uint64_t rhs) {
  BigInt res(*this);
  return res <<= rhs;
}

BigInt BigInt::operator>>(uint64_t rhs) {
  BigInt res(*this);
  return res / BigInt(pow(2, rhs));
}
