#ifndef BIGINT_H
#define BIGINT_H

#include <vector>
#include <string>
#include <cstdint>
#include <tuple>

class BigInt {
  public:
    BigInt(std::string s);
    BigInt(uint64_t x);

    operator std::string() const;
    BigInt operator/(const BigInt &rhs) const;
    BigInt operator/(uint64_t rhs) const;
    BigInt operator%(const BigInt &rhs) const;
    BigInt operator*(const BigInt &rhs) const;
    BigInt operator-(const BigInt &rhs) const;
    BigInt operator+(const BigInt &rhs) const;

    bool operator>=(const BigInt &rhs) const;
    bool operator==(const BigInt &rhs) const;
    bool operator<(const BigInt &rhs) const;
    bool operator<(uint64_t rhs) const;
    bool operator>(const BigInt &rhs) const;
    bool operator>(uint64_t rhs) const;
    bool operator<=(const BigInt &rhs) const;
    bool operator!=(const BigInt &rhs) const;
    bool friend operator<(uint64_t lhs, const BigInt &rhs);

    BigInt const & operator--();
    BigInt const & operator++();
    BigInt const & operator<<=(uint64_t rhs);
    BigInt operator<<(uint64_t rhs);
    BigInt operator>>(uint64_t rhs);

    std::vector<uint8_t>& data() {return _data;}

  private:
    BigInt();
    std::vector<uint8_t> _data;
    std::tuple<BigInt, BigInt> devide(const BigInt &rhs) const;
};

#endif /*BIGINT_H*/
