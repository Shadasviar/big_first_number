#ifndef BIGINT_RANGE_H
#define BIGINT_RANGE_H

#include <iterator>
#include <bigint.h>

class BigIntRangeIterator
    : public std::iterator
    < std::random_access_iterator_tag
    , BigInt
    , BigInt
    >
{
  public:
    BigIntRangeIterator(uint64_t x):BigIntRangeIterator(BigInt(x)){}
    BigIntRangeIterator(const BigInt &x): _begin(0), _end(x), _it(x){}
    BigIntRangeIterator(const BigInt &b, const BigInt &e)
      : _begin(b), _end(e), _it(b) {}

    bool operator!=(const BigIntRangeIterator &rhs) const;
    bool operator==(const BigIntRangeIterator &rhs) const;
    bool operator<(const BigIntRangeIterator &rhs) const;
    bool operator>=(const BigIntRangeIterator &rhs) const;
    friend bool operator<(uint64_t lhs, const BigIntRangeIterator &rhs);

    BigInt const & operator*() const;
    BigInt operator[](const BigInt &rhs) const;
    BigInt operator-(const BigIntRangeIterator &rhs) const;

    BigIntRangeIterator const & operator++();
    BigIntRangeIterator const & operator+=(const BigInt &rhs);

    BigIntRangeIterator operator+(const BigInt &rhs) const;

    BigIntRangeIterator begin() const;
    BigIntRangeIterator end() const;

  private:
    BigInt _begin, _end, _it;
};

#endif /*BIGINT_RANGE_H*/
